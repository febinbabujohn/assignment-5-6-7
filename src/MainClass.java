import java.util.ArrayList;
import java.util.Date;

public class MainClass {
	public static void main(String[] args) {
		
		
		Customer kiran = new Customer(1, "Kiran", "Joseph", "Kiran@gmail.com");

		Bill b1 = new Hydro(1, new Date(), "Hydro", 40.25, "HydroOne", 25);
		Bill b2 = new Internet(2, new Date(), "Internet", 36.20, "Fido", 300);

		ArrayList<Bill> bills = new ArrayList<>();
		bills.add(b2);
		bills.add(b1);
		kiran.setBills(bills);

		Customer amritha = new Customer(2, "Amritha", "Jacob", "amritha@gmail.com");

		Bill b3 = new Hydro(1, new Date(), "Hydro", 35.35, "Enbridge", 25);
		Bill b4 = new Internet(2, new Date(), "Internet", 50.50, "Rogers", 400);
		Bill b5 = new Mobile(3, new Date(), "Mobile", 245.69, "Samsung", "Prepaid Talk + Unlimited Text plan",
				437852963, 2, 260);
		Bill b6 = new Mobile(4, new Date(), "Mobile", 320.78, "One Plus", "LTE+4G 10GB Data Promo plan",
				437854589, 6, 150);

		ArrayList<Bill> bills1 = new ArrayList<>();
		bills1.add(b6);
		bills1.add(b4);
		bills1.add(b5);
		bills1.add(b3);

		amritha.setBills(bills1);

		Customer febin = new Customer(3, "Febin", "John", "febin@gmail.com");

		CustomerStorage cs = new CustomerStorage();

		cs.addCustomer(febin);
		cs.addCustomer(kiran);
		cs.addCustomer(amritha);

		cs.display();

		System.out.println("\n\n\n***********************************************************************");
		System.out.println("************ 	            Searching a customer      	         ***********");
		System.out.println("***********************************************************************");
		
		
		// Search customer
		Customer c1 = cs.getCustomerById(1);
		c1.display();

		System.out.println("\n\n\n***********************************************************************");
		System.out.println("************    Searching a customer that does't exists     ***********");
		System.out.println("***********************************************************************");

		cs.getCustomerById(10);
	}
}
