
	import java.util.Date;

	public class Bill implements IDisplay {
		
		private int billId;
		private Date billDate;
		private String billType;
		private double billAmount;

		public Bill(int billId, Date billDate, String billType, double billAmount) {
			this.billId = billId;
			this.billDate = billDate;
			this.billType = billType;
			this.billAmount = billAmount;
		}
		public int getBillId() {
			return billId;
		}

		public void setBillId(int billId) {
			this.billId = billId;
		}

		public Date getBillDate() {
			return billDate;
		}

		public void setBillDate(Date billDate) {
			this.billDate = billDate;
		}

		public String getBillType() {
			return billType;
		}

		public void setBillType(String billType) {
			this.billType = billType;
		}

		public double getBillAmount() {
			return billAmount;
		}

		public void setBillAmount(double billAmount) {
			this.billAmount = billAmount;
		}

		@Override
		public void display() {
			System.out.println("\tBill Id: " + billId);
			System.out.println("\tBill Date: " + billDate);
			System.out.println("\tBill Type: " + billType);
			System.out.println("\tBill Amount: $" + billAmount);
		}

	}

	

